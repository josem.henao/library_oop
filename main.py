from domain.author import Author
from domain.book import Book
from domain.category import Category
from domain.library import Library


def main():
    biblioteca_uco = Library(name='Biblioteca de Ciencia')
    biblioteca_udea = Library(name='Biblioteca del Conocimiento')

    category_novel = Category(category_name='Novel', description='Fiction narratives')
    gabo = Author(name='Gabriel Garcia Marquez', nationality='Colombian')

    libro_1 = Book(title='Cien años de Soledad', category=category_novel, author=gabo)

    biblioteca_uco.register_book(book=libro_1)
    biblioteca_uco.register_book(book=libro_1)

    # como usuario quiero poder consultar un libro por el titulo
    books = biblioteca_uco.get_resource_by_title(title='Cien Años de Soledad')

    # como usuario quiero poder pedir prestado un libro
    if books:
        loan_status = biblioteca_uco.loan_book(book=books[0])
        print(loan_status)
    # como usuario quiero poder consultar los libros de un autor
    # como usuario quiero poder consultar los libros de una categoria


if __name__ == '__main__':
    main()
