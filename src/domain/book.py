from domain.author import Author
from domain.resource import Resource


class Book(Resource):
    """
    Entity that represents a Book with his author and category
    - book_id
    - author
    - title
    - category
    """

    def __init__(self, title, author: Author, category):
        super().__init__(title, category)
        self._author = author

    @property
    def author(self):
        return self._author

    @author.setter
    def author(self, author):
        self._author = author
