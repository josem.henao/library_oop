from domain.category import Category


class Resource:
    def __init__(self, title: str, category: Category):
        self._id = None  # Autogenerate
        self._title = title
        self._category = category

    @property
    def id(self):
        return self._id

    @property
    def title(self):
        return self._title

    @property
    def category(self):
        return self._category
