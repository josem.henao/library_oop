class Library:
    """
    Entity that represents an entity of a Library which contains books and magazines
    - library_id
    - name
    - location
    - books
    - magazines
    """

    def __init__(self, name, books=None, magazines=None):
        self._library_id = None  # TODO
        self._name = name
        self._books: list = books if books else []
        self._magazines: list = magazines if magazines else []
        self._borrowed_books = []
        self._borrowed_magazines = []

    def get_resource_by_title(self, title: str):
        """

        :param title: str, the title of the resource to look for
        :return: list. The list of matching resources
        """
        match_books = list(filter(lambda x: x.title.lower() == title.lower(), self._books))
        match_magazines = list(filter(lambda x: x.title.lower() == title.lower(), self._magazines))

        return [*match_books, *match_magazines]

    def register_book(self, book):
        """

        :param book:
        :return:
        """
        self._books.append(book)

    def loan_book(self, book):
        """

        :param book:
        :return:
        """
        # TODO: validate that the books exists
        self._books.remove(book)
        self._borrowed_books.append(book)
