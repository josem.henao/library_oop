from datetime import datetime

from domain.category import Category
from domain.resource import Resource


class Magazine(Resource):
    """
    Entity that represents a periodic magazine
    publish_date
    serial_number
    title
    category
    """

    def __init__(self, title: str, category: Category, publish_date: datetime, serial_number: str):
        super().__init__(title, category)
        self._publish_date = publish_date
        self._serial_number = serial_number
